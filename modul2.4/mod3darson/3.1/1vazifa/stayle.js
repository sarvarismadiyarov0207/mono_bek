// let a = 20;
// let b = 40;
// let S = a * b;
// console.log(S);

// Hosting
// temporar dead zone
// console.log(me);

// var me = "Sarvar";

// ==============================================================
// Function -------

// let a = {
//   b: {
//     c: {
//       d: this,
//     },
//   },
// };
// console.log(a.b.c);

// const obj = {
//   a: 24,
//   b: {
//     c: 35,
//   },
// };

// const obj8 = {
//   d: 8,
//   x: {
//     f: 2,
//   },
// };

// const b2 = JSON.stringify(obj8);

// const obj2 = JSON.parse(b2);
// console.log(obj2);
// obj2.x.f = 5;

// const obj2 = Object.assign({}, obj);
// obj2.a = 26;
// obj2.a.c = 36;

// spread operators
// const obj3 = { ...obj, ...obj8 };
// obj3.a = 28;

// console.log(obj3.a);
// console.log("all", obj);
// console.log(obj2.a);

// =============16.12.2021======================
// DistraKCHIL

// const arr = [2, 3, 4, 5, 6, 7];
// const a = arr[0];
// const b = arr[1];
// const c = arr[2];

// const [x, y, ...others] = arr;
// console.log(others);
// const t = [...arr];
// console.log(x, y);
// console.log(arr);

// console.log(a, b, c);

// // Swaping varilabs
// let temp;
// temp = c;
// c = d;
// d = temp;
// console.log(c, d);

// Default values
// const [p = 1, q = 3, r = 5] = [7, 7, 7];
// console.log(p, q, r);

// const openingHours = {
//   morning: 8,
//   afternoon: 12,
//   night: 14,
// };

// ==================>17.12.2021<================================

// =================object.keys(), object.values(), object.entries()=========
// const openingHours = {
//     morning: 8,
//     afternoon: 12,
//     night: 24,
//   };

//   console.log(".", openingHours.morning); // 8
//   console.log("[]", openingHours["morning"]); //8

//   // Property NAMES
//   const properties = Object.keys(openingHours);
//   console.log(properties);
//   console.log(typeof properties);

//   for (let i of properties) {
//     console.log("key", typeof i);
//     console.log("open", openingHours[i]);
//   }

//   // Property NAMES
//   const values = Object.values(openingHours);
//   console.log("values", values);
//   console.log(typeof values);

//   // Property NAMES
//   const entry = Object.entries(openingHours);
//   console.log("entry", entry);
//   console.log(typeof entry);

//   for (let i of entry) {
//     console.log("entry", i);
//     console.log(typeof i);
//     console.log("", i);
//     console.log("array", i[0]);
//   }
// ===========================================
// 1. Scope chaining
// 2. Variables  env
// 3. this

// const a = 5;
// function add(a, b) {
//   const c = 5;
//   minus(c, b);
// }

// const minus = function (c, b) {
//   mult(c);
// };

// const mult = (b) => {
//   console.log("this", this);
//   return a * b;
// };

// add(a, 5);
// mult(5);

// let obj = {
//   method1() {
//     console.log(this);
//     // console.log("salom");
//   },
//   method2: () => {
//     console.log(this);
//   },
//   method3: () => {
//     let func = () => console.log(this);
//     func();
//   },
//   method5: function () {
//     function add(a) {
//       this.a = a;
//       console.log("inFunc", this.a);
//       let cons = () => {
//         console.log("inArrow", this.a);
//       };
//       cons();
//     }
//     add(5);
//   },

//   method4() {
//     let func = () => console.log(this);
//     func();
//   },
// };

// obj.method1();
// obj.method2();
// obj.method3();
// obj.method4();
// obj.method5();
// =========================================================
// contexlar uchun
// const a = 5;
// function add(a, b) {
//   return minus(a, b);
// }
// const minus = function (a, b) {
//   const c = 6;
//   return mult(c);
// };
// const mult = (c) => {
//   return 5 * c;
// };
// const r = add();
// =================================================

// =================Method
// // Arrays
// const users = [{ name: undefined, email: "" }];
// // const users = [];

// console.log(users[0]?.name ?? "User array empty");

// if (users[0].name !== undefined) {
//   console.log("User array empty");
// }

// if (users.length > 0) console.log("this", users[1]?.firstName);
// else console.log("user array empty");

//==================> Sets <============================
// const arr = ["Pasta", "Pizza", "Pizza", "Risotto", "Pasta", "Pizza"];

// const ordersSet = new Set([
//   "Pasta",
//   "Pizza",
//   "Pizza",
//   "Risotto",
//   "Pasta",
//   "Pizza",
// ]);
// console.log(ordersSet);
// console.log(new Set(arr));
// console.log(arr);

// // console.log(new Set(''));

// const nums = new Set([1, 2, 3, 4, 4, 4, 9]);
// nums.delete(4);
// console.log(nums);
// console.log("nums", nums);

// console.log(ordersSet.size);
// console.log(ordersSet.has("Pizza"));
// // console.log(ordersSet.has('Bread'));
// ordersSet.add("Garlic Bread");
// // ordersSet.add('Garlic Bread');
// ordersSet.delete("Risotto");
// // ordersSet.clear();
// console.log(ordersSet);

// for (const order of ordersSet) {
//   console.log("iterate", order);
// }

// // // Example
// const staff = ["Waiter", "Chef", "Waiter", "Manager", "Chef", "Waiter"];
// const staffUnique = [...new Set(staff)];
// console.log(staffUnique);

// for (let i = 0; i < 5; i++) {
//   text += "The number is " + i + "<br>";
// }

// let answer = Number;

// // test loop
// while (true) {
//   let guess = Number;

//   if (answer < guess) {
//     console.log(guess + ">");
//   } else if (answer > guess) {
//     console.log(guess + "<");
//   } else if (guess == answer) {
//     console.log("Siz to'gri topdingiz!");
//     break;
//   }
// }


let random = Math.trunc(Math.random() * 20 + 1);
console.log(random);
let score = 20;
let highScore = 0;

const randomNumber = document.querySelector(".random-number");
const inputNumber = document.querySelector(".input-number");
const notification = document.querySelector(".notification");
const checkBtn = document.querySelector(".check-btn");

const highScoreSpan = document.querySelector(".high-score");
highScoreSpan.textContent = highScore;
const resetBtn = document.querySelector(".reset-btn");
const scoreSpan = document.querySelector(".score");
scoreSpan.textContent = score;

inputNumber.addEventListener("change", (e) => {
  if (e.target.value) {
    checkBtn.disabled = false;
  } else {
    checkBtn.disabled = true;
  }
});

checkBtn.addEventListener("click", () => {
  const val = inputNumber.value;

  if (val) {
    if (val > random) {
      notification.textContent = ${val}>;
      notification.style.opacity = "1";
      score--;
      scoreSpan.textContent = score;
    } else if (val < random) {
      notification.textContent = ${val}<;
      notification.style.opacity = "1";
      score--;
      scoreSpan.textContent = score;
    } else {
      notification.textContent = "Siz to'gri toptingiz!";
      document.body.style.backgroundColor = "green";
      randomNumber.textContent = random;

      if (highScore < score) {
        highScore = score;
      }

      highScoreSpan.textContent = highScore;
    }
  }
});

resetBtn.addEventListener("click", () => {
  document.body.style.backgroundColor = "coral";
  score = 20;
  scoreSpan.textContent = score;
  inputNumber.value = "";
  randomNumber.textContent = "?";
  notification.textContent = "salom";
  notification.style.opacity = "0";
  random = Math.trunc(Math.random() * 20 + 1);
});

// Aksisbilete

